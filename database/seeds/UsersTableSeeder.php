<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password01!'),
            'created_at' => '2018-03-03 08:38:43',
            'updated_at' => '2018-03-03 08:38:43'
        ]);
    }
}
